
JAVA_HOME=/usr/jdk64/jdk1.8.0_112
HADOOP_HOME=${HADOOP_HOME:-/usr/hdp/2.6.5.1100-53/hadoop}

if [ -d "/usr/lib/tez" ]; then
  PIG_OPTS="$PIG_OPTS -Dmapreduce.framework.name=yarn"
fi